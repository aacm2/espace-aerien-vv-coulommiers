#!/bin/bash
if [ $# -lt 1 ]; then
    echo "Usage: $0 <source>" >&2
    exit 1
fi

source="$1"
mkdir -p gen/old/
mv gen/*.txt gen/old/
for p in {SP,CBASE,CEL4500,CEL55} ; do
    patch source/${source} -i patch/${p}.diff -o gen/${source%.*}.${p}.txt
    diff -U 5 source/${source} gen/${source%.*}.${p}.txt > patch/${p}.diff
done
