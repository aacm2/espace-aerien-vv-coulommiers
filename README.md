# Espace aérien avec les zones VV de Coulommiers
L'objectif est de fournir des fichiers au format Openair (pour XCSoar notamment) de l'espace aérien prenant en compte l'activation des zones et notamment la substitution des portions de l'espace aérien de classe A par les zones VV. Ainsi, la lecture de la carte devient plus claire et il n'est plus nécessaire de désactiver les alertes en cas de pénétration dans la classe A.

## Fichiers générés
* Espace aérien sans protocole : [1903_AIRSPACE_France_DP.SP.txt](./gen/1903_AIRSPACE_France_DP.SP.txt)
* Espace aérien CBASE : [1903_AIRSPACE_France_DP.CBASE.txt](./gen/1903_AIRSPACE_France_DP.CBASE.txt)
* Espace aérien CEL4500 : [1903_AIRSPACE_France_DP.CEL4500.txt](./gen/1903_AIRSPACE_France_DP.CEL4500.txt)
* Espace aérien CEL55 : [1903_AIRSPACE_France_DP.CEL55.txt](./gen/1903_AIRSPACE_France_DP.CEL55.txt)

## Rappels
Pendant l'activation de la zone : 
* IFR et CAM : contournement obligatoire.
* VFR : pénétration strictement réservée aux pilotes des aéroclubs signataires d'un protocole avec l'organisme gestionnaire et à leurs invités dûment instruits. 
Cette zone se substitue aux portions de l'espace aérien de classe A avec lesquelles elle interfère. 

## Sources
### SIA / eAIP en vigueur (28 Mar 2019) / ENR 5.1 (ZONES INTERDITES, REGLEMENTEES ET DANGEREUSES)
* LF R 3103 : https://www.sia.aviation-civile.gouv.fr/dvd/eAIP_28_MAR_2019/FRANCE/AIRAC-2019-03-28/html/eAIP/FR-ENR-5.1-fr-FR.html

### Fichier FFVP « Espace Aérien France »
* Présentation : https://www.ffvp.fr/kb/fichier-airspace-pour-les-gps
* Fichier au format Openair (28/03/19) : https://drive.google.com/file/d/1Zaro_YO_YhD4Ug3AmGYlc-PSZJC71xeG/view
* Modification réalisée par Dominique PERNOT (pour corriger les bugs dans XCSoar notamment) : [1903_AIRSPACE_France_DP.txt](./source/1903_AIRSPACE_France_DP.txt)

### Protocole des zones VV de Coulommiers
* https://www.planeur-aacm.net/images/docs/zones/120408__PROTOCOLE_Protocole_Coulommier_CDG_ORY.pdf

## Générer les fichiers
Lorsque la publication d'information aéronautique (AIP) est mise à jour, il convient de mettre à jour les fichiers de l'espace aérien.
Il devrait être possible de récupérer le fichier « Espace Aérien France » mis à jour (le placer dans le dossier `source`). À partir de ce fichier et des fichiers *diff*, il devrait être possible de générer les fichiers par protocole :
```
./build.sh "1903_AIRSPACE_France_DP.txt"
```
